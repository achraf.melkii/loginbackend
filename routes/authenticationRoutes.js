
const mongoose = require('mongoose');
const Account = mongoose.model('accounts');


module.exports = app =>{



    //routes
    //using get method
    app.get('/account',async (req,res)=>{

        const{ rUsername, rPassword } = req.query;
        if(rUsername == null || rPassword == null)
        {
            res.send("Invalid credentials");
            return;
        }

        var userAccount = await Account.findOne({username : rUsername});//must add await to wait for the response from the data base
        console.log(userAccount);
            if (userAccount == null){
                //create new account
                console.log('create new account..')

                var newAccount = new Account({
                    username : rUsername,
                    password : rPassword,
                    lastAuthentication :Date.now()

                });

                await newAccount.save();

                res.send(newAccount);
                return;

        }
        else{
            if (rPassword == userAccount.password){ // in production the password should be incripted not just a password = password
                userAccount.lastAuthentication = Date.now();
                await userAccount.save();

                res.send(userAccount);
                console.log("retrieving account ..")
                
                return;
            }
        }

        res.send("Invalid credentials");
        return;



        //res.send("hello world its "+Date.now());
        });

}