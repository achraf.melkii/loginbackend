const express = require('express');
const keys = require('./config/keys.js');


console.log('helloo');

const app = express();


//setting up DB 
const mongoose = require('mongoose');
mongoose.connect(keys.mongoURI);

//ssetup database models
require('./model/Account')


//setup routes
require('./routes/authenticationRoutes')(app);


app.listen(keys.port,()=>{
    console.log("listening on "+ keys.port);
}); 